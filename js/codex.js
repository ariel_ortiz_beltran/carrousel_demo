var srcArray = new Array();
var captionArray = new Array();
var translateArray = new Array();
var position = 0;

$(function(){

	srcArray[0] = '../assets/codex/c1675-05.jpg';
	captionArray[0] = "Puzzle initial 'C'(redo), at the beginning of Pseudo-Seneca's Epistolae to Paul. (I)";
	translateArray[0] = "Puzzle inicial 'C'(redo), al comienzo de la epístola epígrafe de Seneca a Pablo (I)"
	srcArray[1] = '../assets/codex/c1675-05a.jpg';
	captionArray[1] = "Puzzle initial 'C'(redo), at the beginning of Pseudo-Seneca's Epistolae to Paul. (II)";
	translateArray[1] = "Puzzle inicial 'C'(redo), al comienzo de la epístola epígrafe de Seneca a Pablo (IIS)"
	srcArray[2] = '../assets/codex/c1675-06.jpg';
	captionArray[2] = "Puzzle initial 'A'(nneus), at the beginning of the Spanish translation of Pseudo-Seneca's Epistolae to Paul. (I)";
	translateArray[2] = "Puzzle inicial 'A'(nneus), al comienzo de la  traducción Española de la epístola epígrafe de Seneca a Pablo (I)"
	srcArray[3] = '../assets/codex/c1675-06a.jpg';
	captionArray[3] = "Puzzle initial 'A'(nneus), at the beginning of the Spanish translation of Pseudo-Seneca's Epistolae to Paul. (II)";
	translateArray[3] = "Puzzle inicial 'C'(redo), al comienzo de la  traducción Española de la epístola epígrafe de Seneca a Pablo (II)"
	$("#image").attr('src', srcArray[0]);
	$("#image").after('<figcaption onmouseover="mouseOver()" onmouseout="mouseOut()">'+captionArray[0]+'</figcaption>');
});

function previous(){
	$('figcaption').html("");
	if(position > 0){
		position--;
	}
	$("#image").attr('src', srcArray[position]);
	$("#image").after('<figcaption onmouseover="mouseOver()" onmouseout="mouseOut()">'+captionArray[position]+'</figcaption>');
}

function next(){
	$('figcaption').html("");
	if(position < srcArray.length -1){
		position++;
	}
	$("#image").attr('src', srcArray[position]);
	$("#image").after('<figcaption onmouseover="mouseOver()" onmouseout="mouseOut()">'+captionArray[position]+'</figcaption>');
}

function mouseOver(){
	$('figcaption').html("");
	$("#image").after('<figcaption onmouseover="mouseOver()"  onmouseout="mouseOut()">'+translateArray[position]+'</figcaption>');
}

function mouseOut(){
	$('figcaption').html("");
	$("#image").after('<figcaption onmouseover="mouseOver()" onmouseout="mouseOut()">'+captionArray[position]+'</figcaption>');
}